//
//  AppModule.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//  Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

enum AppModules: String, ViperitModule {
    case login
    case home
}
