//
//  PostDetailPresenter.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - PostDetailPresenter Class
final class PostDetailPresenter: Presenter {
    var postID: String = ""
    override func setupView(data: Any) {
        self.postID = (data as AnyObject).allKeys[0] as! String
    }
    
    override func viewHasLoaded() {
        super.viewHasLoaded()
        interactor.getDataInteractor(postID: self.postID){
            response in
            self.view.showData(self.postID, getObj: response as! NSMutableDictionary)
        }
    }
}

// MARK: - PostDetailPresenter API
extension PostDetailPresenter: PostDetailPresenterApi {
    func postEditing(obj: NSDictionary) {
        router.showEditRoute(obj: obj)
    }
    
}

// MARK: - PostDetail Viper Components
private extension PostDetailPresenter {
    var view: PostDetailViewApi {
        return _view as! PostDetailViewApi
    }
    var interactor: PostDetailInteractorApi {
        return _interactor as! PostDetailInteractorApi
    }
    var router: PostDetailRouterApi {
        return _router as! PostDetailRouterApi
    }
}
