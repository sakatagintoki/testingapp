//
//  PostDetailRouter.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - PostDetailRouter class
final class PostDetailRouter: Router {
}

// MARK: - PostDetailRouter API
extension PostDetailRouter: PostDetailRouterApi {
    func showEditRoute(obj: NSDictionary) {
        let module = AppModules.AddPost.build()
        module.router.show(from: _view, embedInNavController: false, setupData: obj)
    }
    
}

// MARK: - PostDetail Viper Components
private extension PostDetailRouter {
    var presenter: PostDetailPresenterApi {
        return _presenter as! PostDetailPresenterApi
    }
}
