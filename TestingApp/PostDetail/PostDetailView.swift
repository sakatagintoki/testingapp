//
//  PostDetailView.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import UIKit
import Viperit
import MapKit
import Firebase
import FirebaseDatabase

//MARK: PostDetailView Class
final class PostDetailView: UserInterface {
    
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var viewDetail: UIView!
    @IBOutlet var lblGasType: UILabel!
    @IBOutlet var lblCapacity: UILabel!
    @IBOutlet var lblPrice1: UILabel!
    @IBOutlet var lblpostType: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDelivery: UILabel!
    @IBOutlet var mapView: MKMapView!
    
    let annotation = MKPointAnnotation()
    let regionRadius: CLLocationDistance = 5000
    var ref: DatabaseReference!
    
    var postObject = NSDictionary()
    var postID: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        let homeLocation = CLLocation(latitude: 16.80528, longitude: 96.15611)
        self.mapView.showsUserLocation = true
        annotation.coordinate = CLLocationCoordinate2D(latitude: 16.80528, longitude: 96.15611)
        mapView.addAnnotation(annotation)
        centerMapOnLocation(location: homeLocation)
        
        let pwdBorder = CALayer()
        pwdBorder.borderColor = UIColor.gray.cgColor
        pwdBorder.frame = CGRect(x: 0, y: self.viewHeader.frame.size.height - 1, width: self.viewHeader.frame.size.width, height: 0.5)
        pwdBorder.borderWidth = 1
        pwdBorder.opacity = 0.4
        self.viewHeader.layer.addSublayer(pwdBorder)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.postObject.count > 0 {
            self.viewDetail.isHidden = false
            let object = self.postObject
            let capacity = object.object(forKey: "capacity") != nil ? object.object(forKey: "capacity") as? String : ""
            let price = object.object(forKey: "price") != nil ? object.object(forKey: "price") as? String : ""
            let delivery = object.object(forKey: "delivery") != nil ? object.object(forKey: "delivery") as? Bool : false
            let created = object.object(forKey: "createdBy") != nil ? object.object(forKey: "createdBy") as! Int : 0
            let postType = object.object(forKey: "postType") != nil ? object.object(forKey: "postType") as! String : "1"
            let createdBy = Int("\(created)")
            let date = Date(timeIntervalSince1970: Double(createdBy!) / 1000)
            let perPrice: Int = (Int(price!)! / Int(capacity!)!)
            self.lblGasType.text = object.object(forKey: "gasType") as? String
            self.lblCapacity.text = "\(capacity!) Litre"
            self.lblPrice1.text = "$ \(price!)"
            self.lblDate.text = "\(Utility().timeAgoSinceDate(date: date as NSDate, numericDates: true))"
            self.lblLocation.text = object.object(forKey: "location") as? String
            self.lblPrice.text = "$ \(perPrice) (Per Litre)"
            if postType == "1" {
                self.lblpostType.text = "Want to Sell"
            } else {
                self.lblpostType.text = "Want to Buy"
            }
            if delivery == true {
                self.lblDelivery.textColor = UIColor.green
            } else {
                self.lblDelivery.textColor = UIColor.red
            }
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    @IBAction func clickEditPost(_ sender: Any) {
        let obj = NSMutableDictionary()
        obj.setObject(self.postObject, forKey: "\(self.postID)" as NSCopying)
        self.presenter.postEditing(obj: obj)
    }
    
    @IBAction func clickDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Delete Post", message: "Are you sure you want to delete", preferredStyle: .alert)
        let OKAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            self.ref.child("posts").child("\(self.postID)").removeValue()
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        alert.addAction(OKAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func clickClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - PostDetailView API
extension PostDetailView: PostDetailViewApi {
    func showData(_ post_id: String, getObj: NSMutableDictionary) {
        self.postObject = getObj as NSDictionary
        self.postID = post_id
    }
}

// MARK: - PostDetailView Viper Components API
private extension PostDetailView {
    var presenter: PostDetailPresenterApi {
        return _presenter as! PostDetailPresenterApi
    }
    var displayData: PostDetailDisplayData {
        return _displayData as! PostDetailDisplayData
    }
}
