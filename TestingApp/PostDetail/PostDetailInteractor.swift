//
//  PostDetailInteractor.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit
import Firebase
import FirebaseDatabase

// MARK: - PostDetailInteractor Class
final class PostDetailInteractor: Interactor {
}

// MARK: - PostDetailInteractor API
extension PostDetailInteractor: PostDetailInteractorApi {
    func getDataInteractor(postID: String, _ complete: @escaping (NSDictionary) -> Void) {
        var ref: DatabaseReference!
        ref = Database.database().reference()
        let recentPostsQuery = (ref?.child("posts").child(postID))
        
        recentPostsQuery?.observe(DataEventType.value, with: { (snapshot) in
            let gasArray = snapshot.value as! NSDictionary
            complete(gasArray)
        })
    }
}

// MARK: - Interactor Viper Components Api
private extension PostDetailInteractor {
    var presenter: PostDetailPresenterApi {
        return _presenter as! PostDetailPresenterApi
    }
}
