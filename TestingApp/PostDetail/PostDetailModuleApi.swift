//
//  PostDetailModuleApi.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Viperit

//MARK: - PostDetailRouter API
protocol PostDetailRouterApi: RouterProtocol {
    func showEditRoute(obj: NSDictionary)
}

//MARK: - PostDetailView API
protocol PostDetailViewApi: UserInterfaceProtocol {
    func showData(_ post_id: String, getObj: NSMutableDictionary)
}

//MARK: - PostDetailPresenter API
protocol PostDetailPresenterApi: PresenterProtocol {
    func postEditing(obj: NSDictionary)
}

//MARK: - PostDetailInteractor API
protocol PostDetailInteractorApi: InteractorProtocol {
    func getDataInteractor(postID: String,_ complete: @escaping (NSDictionary) -> Void)
}
