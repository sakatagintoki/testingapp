//
//  Constants.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//  Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static var colorPrimary: String = "FF5722"
    static var colorPrimaryDark: String = "FF1111"
    static var colorAccent: String = "FF9800"
    
    static let TABLE_POSTS: String = "posts"
    static let KEY_ID: String = "key"
    static let KEY_CAPACITY: String = "capacity"
    static let KEY_DELIVERY_OPTION: String = "delivery"
    static let KEY_GAS_TYPE: String = "gasType"
    static let KEY_LOCATION: String = "location"
    static let KEY_NEGOTIATION: String = "negotiation"
    static let KEY_POST_TYPE: String = "postType"
    static let KEY_PRICE: String = "price"
    static let KEY_USER: String = "user"
    static let KEY_CREATED_DATE: String = "createdBy"
    static let KEY_MODIFIED_DATE: String = "modifiedBy"
    static let KEY_STATUS: String = "status"
    static let KEY_ORDERSTATUS: String = "orderStatus"
    static let KEY_ORDERID: String = "orderid"
    
    static let _base62chars: NSString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
}
