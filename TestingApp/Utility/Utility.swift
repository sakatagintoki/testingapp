//
//  Utility.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//  Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

//MARK: - Application modules
enum AppModules: String, ViperitModule {
    case onboard
    case login
    case home
    case AddPost
    case PostDetail
}

class Utility: NSObject {
    
    /// Set Selected Previous generateUUID
    /// - parameter String: generateUUID
    func setGenerateUUID(_ idUUID: String) {
        let defaults = UserDefaults.standard
        defaults.set(idUUID, forKey: "GenerateUUID")
    }
    
    // get UUID
    /// - Returns: A uuidstring String to the `GenerateUUID`.
    func getGenerateUUID() -> String {
        var uuid: String = ""
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: "GenerateUUID") != nil) {
            uuid = defaults.object(forKey: "GenerateUUID") as! String
        }
        return uuid
    }
    
    /// Document Directory
    /// - Returns: A Document Directory String to the `getDocumentDirectory`.
    func getDocumentDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask, true)
        let docPath = paths[0]
        return docPath as NSString
    }
    
    /// Sync Directory
    /// - Returns: A Contents Directory String to the `getContentsDirectory`.
    func getSyncDirectory() -> NSString {
        let docPath = self.getDocumentDirectory()
        let syncDirectory = docPath.appendingPathComponent("Sync/")
        return syncDirectory as NSString
    }
    
    // Show Alert Message
    /// - Returns: A UIViewController to the `showAlert`.
    func showAlert(_ title: String, message: String, btnText: String) -> UIViewController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: btnText, style: .cancel) { action -> Void in
        }
        alert.addAction(cancelAction)
        return alert
    }
    
    // show time ago
    /// - Returns: A time ago String to the `timeAgoSinceDate`.
    func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
