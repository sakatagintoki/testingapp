//
//  LoginModuleApi.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Viperit

//MARK: - LoginRouter API
protocol LoginRouterApi: RouterProtocol {
    func showHomeRoute()
}

//MARK: - LoginView API
protocol LoginViewApi: UserInterfaceProtocol {
}

//MARK: - LoginPresenter API
protocol LoginPresenterApi: PresenterProtocol {
    func showHomePresenter()
}

//MARK: - LoginInteractor API
protocol LoginInteractorApi: InteractorProtocol {
}
