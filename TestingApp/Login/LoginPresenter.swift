//
//  LoginPresenter.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - LoginPresenter Class
final class LoginPresenter: Presenter {
}

// MARK: - LoginPresenter API
extension LoginPresenter: LoginPresenterApi {
    func showHomePresenter() {
        router.showHomeRoute()
    }
}

// MARK: - Login Viper Components
private extension LoginPresenter {
    var view: LoginViewApi {
        return _view as! LoginViewApi
    }
    var interactor: LoginInteractorApi {
        return _interactor as! LoginInteractorApi
    }
    var router: LoginRouterApi {
        return _router as! LoginRouterApi
    }
}
