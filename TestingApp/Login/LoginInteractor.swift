//
//  LoginInteractor.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - LoginInteractor Class
final class LoginInteractor: Interactor {
    
}

// MARK: - LoginInteractor API
extension LoginInteractor: LoginInteractorApi {
}

// MARK: - Interactor Viper Components Api
private extension LoginInteractor {
    var presenter: LoginPresenterApi {
        return _presenter as! LoginPresenterApi
    }
}
