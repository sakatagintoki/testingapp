//
//  LoginView.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import UIKit
import Viperit
import Firebase

//MARK: LoginView Class
final class LoginView: UserInterface {
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLogin()
    }
    
    @IBAction func clickButton(_ sender: Any) {
        if !(self.txtUsername.text?.isEmpty)! && !(self.txtPassword.text?.isEmpty)! {
            displayData.testingLogin(username: self.txtUsername.text!, password: self.txtPassword.text!){
                response in
                print(response)
                if response == true {
                    self.presenter.showHomePresenter()
                } else {
                    let alert = Utility().showAlert("Login Fail", message: "Authorisation unsuccessful. Check that you have entered details correctly & retry.", btnText: "OK")
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            let alert = Utility().showAlert("Login Fail", message: "Authorisation unsuccessful. Check that you have entered details correctly & retry.", btnText: "OK")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func viewLogin(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: self.txtUsername.frame.size.height - width, width: self.txtUsername.frame.size.width, height: 0.5)
        border.borderWidth = width
        self.txtUsername.layer.addSublayer(border)
        let placeHolder = NSAttributedString(string: "Username", attributes: [NSAttributedStringKey.foregroundColor : UIColor.gray])
        self.txtUsername.attributedPlaceholder = placeHolder
        
        let pwdBorder = CALayer()
        pwdBorder.borderColor = UIColor.gray.cgColor
        pwdBorder.frame = CGRect(x: 0, y: self.txtPassword.frame.size.height - width, width: self.txtPassword.frame.size.width, height: 0.5)
        pwdBorder.borderWidth = width
        self.txtPassword.layer.addSublayer(pwdBorder)
        let pwdPH = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor : UIColor.gray])
        self.txtPassword.attributedPlaceholder = pwdPH
        
        buttonLogin.layer.cornerRadius = 8
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - LoginView API
extension LoginView: LoginViewApi {
}

// MARK: - LoginView Viper Components API
private extension LoginView {
    var presenter: LoginPresenterApi {
        return _presenter as! LoginPresenterApi
    }
    var displayData: LoginDisplayData {
        return _displayData as! LoginDisplayData
    }
}
