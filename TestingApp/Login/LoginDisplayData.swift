//
//  LoginDisplayData.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit
import Firebase
import FirebaseDatabase

// MARK: - LoginDisplayData class
final class LoginDisplayData: DisplayData {
    var ref: DatabaseReference!
    // Dummy Data and Api Request
    
    // although this func can write in loginView, I created as a new function
    func testingLogin(username: String, password: String, complete: @escaping (Bool) -> Void){
        Auth.auth().signIn(withEmail: username, password: password) { (user, error) in
            if error != nil {
                complete(false)
            } else {
                complete(true)
            }
        }
    }
}
