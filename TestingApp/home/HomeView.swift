//
//  HomeView.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import UIKit
import Viperit
import Firebase

//MARK: HomeView Class
final class HomeView: UserInterface {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet var indicatorIcon: UIActivityIndicatorView!
    @IBOutlet var viewNoPost: UIView!
    
    let obj:[String : Any] = [:]
    var postObj: NSArray = []
    var postTitle: NSArray = []
    var postList: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let sync = Utility().getSyncDirectory()
        let firebaseAuth = Auth.auth()
        let user = firebaseAuth.currentUser
        let uuid = (user?.uid)! as String
        let dbname = NSString(string: "\(uuid).db") as String
        let setting_db = sync.appendingPathComponent("\(dbname)")
        let source_localdb = (Bundle.main.resourcePath! as NSString).appendingPathComponent("postsManager.db")
        do {
            if !FileManager.default.fileExists(atPath: sync as String){
                try FileManager.default.createDirectory(atPath: sync as String, withIntermediateDirectories: true, attributes: nil)
            }
        } catch let error as NSError {
            print("Unable to create1 directory \(error.debugDescription)")
        }
        if !FileManager.default.fileExists(atPath: setting_db){
            do {
                try FileManager.default.copyItem(atPath: source_localdb, toPath: setting_db as String)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error)")
            }
        }
        
        let pwdBorder = CALayer()
        pwdBorder.borderColor = UIColor.gray.cgColor
        pwdBorder.frame = CGRect(x: 0, y: self.viewHeader.frame.size.height - 1, width: self.viewHeader.frame.size.width, height: 0.5)
        pwdBorder.borderWidth = 1
        pwdBorder.opacity = 0.4
        self.viewHeader.layer.addSublayer(pwdBorder)
        
        self.buttonAdd.layer.cornerRadius = 22
        self.buttonAdd.layer.shadowOpacity = 0.7
        self.buttonAdd.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.buttonAdd.layer.shadowRadius = 2.0
        self.buttonAdd.layer.shadowColor = UIColor.darkGray.cgColor
    }
    
    @IBAction func clickAdd(_ sender: Any) {
        self.presenter.postAdding(obj: obj as NSDictionary)
    }
    
    @IBAction func clickLogout(_ sender: Any) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout", preferredStyle: .alert)
        let OKAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            self.presenter.showLoginPresenter()
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        alert.addAction(OKAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - HomeView API
extension HomeView: HomeViewApi {
    func getData(_ list: NSDictionary){
        if list.count > 0 {
            let dataList = list.allValues as NSArray
            self.postObj = dataList.reversed() as NSArray
            let keyList = list.allKeys as NSArray
            self.postList = keyList.reversed() as NSArray
            self.indicatorIcon.isHidden = true
            self.viewNoPost.isHidden = true
            self.tableView.isHidden = false
            self.tableView.reloadData()
        } else {
            self.indicatorIcon.isHidden = true
            self.tableView.isHidden = true
            self.viewNoPost.isHidden = false
        }
        
    }
}

extension HomeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! hCell
        let row = self.postObj[indexPath.row] as! NSDictionary
        let capacity = row.object(forKey: "capacity") as? String
        let price = row.object(forKey: "price") as? String
        let created = row.object(forKey: "createdBy") as! Int
        let createdBy = Int("\(created)")
        let date = Date(timeIntervalSince1970: Double(createdBy!) / 1000)
        cell.lblCategory.text = row.object(forKey: "gasType") as? String
        cell.lblAmount.text = "\(capacity!) Litre"
        cell.lblPrice.text = "$ \(price!)"
        cell.lblDate.text = "\(Utility().timeAgoSinceDate(date: date as NSDate, numericDates: true))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = NSMutableDictionary()
        let object = self.postObj[indexPath.row] as! NSDictionary
        let post_id = self.postList[indexPath.row] as! String
        obj.setObject(object, forKey: "\(post_id)" as NSCopying)
        self.presenter.showPostDetails(obj: obj)
    }
}

// MARK: - HomeView Viper Components API
private extension HomeView {
    var presenter: HomePresenterApi {
        return _presenter as! HomePresenterApi
    }
    var displayData: HomeDisplayData {
        return _displayData as! HomeDisplayData
    }
}
