//
//  HomeInteractor.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - HomeInteractor Class
final class HomeInteractor: Interactor {

}

// MARK: - HomeInteractor API
extension HomeInteractor: HomeInteractorApi {
    func getGasInteractor() {
        PostDataManager().getPostList(){
            response in
            // save the data to local
//            let keyArray = response.allKeys as! [String]
//            for i in 0..<keyArray.count {
//                let sectionRow = keyArray[i]
//                let row = response.object(forKey: sectionRow) as! NSDictionary
//                PostDataManager().savePost(sectionRow, posts: row)
//            }
            self.presenter.getGasPresenter(obj: response)
        }
    }
}

// MARK: - Interactor Viper Components Api
private extension HomeInteractor {
    var presenter: HomePresenterApi {
        return _presenter as! HomePresenterApi
    }
}
