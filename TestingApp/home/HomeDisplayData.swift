//
//  HomeDisplayData.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit
import Firebase
import FirebaseDatabase
import SQLite

// MARK: - HomeDisplayData class
final class HomeDisplayData: DisplayData {
    
}

class PostDataManager: NSObject {
    var ref: DatabaseReference!
    func getPostList(_ complete: @escaping (NSDictionary) -> Void) {
        ref = Database.database().reference()
        let recentPostsQuery = (ref?.child("posts").queryOrdered(byChild: "createdBy"))
        
        recentPostsQuery?.observe(DataEventType.value, with: { (snapshot) in
            let gasArray = snapshot.value as! NSDictionary
            complete(gasArray)
        })
    }
    
    func savePost(_ postID: String, posts: NSDictionary){
        let sync = Utility().getSyncDirectory()
        let firebaseAuth = Auth.auth()
        let user = firebaseAuth.currentUser
        let uuid = (user?.uid)! as String
        let dbname = NSString(string: "\(uuid).db") as String
        let setting_db = sync.appendingPathComponent("\(dbname)")
        do {
            let db = try Connection(setting_db)
            let postTable = Table("\(Constants.TABLE_POSTS)")
            let keyC = Expression<String?>("\(Constants.KEY_ID)")
            let capacityC = Expression<Int?>("\(Constants.KEY_CAPACITY)")
            let deliveryC = Expression<Bool?>("\(Constants.KEY_DELIVERY_OPTION)")
            let gasTypeC = Expression<String?>("\(Constants.KEY_GAS_TYPE)")
            let locationC = Expression<String?>("\(Constants.KEY_LOCATION)")
            let negotiationC = Expression<Bool?>("\(Constants.KEY_NEGOTIATION)")
            let postTypeC = Expression<String?>("\(Constants.KEY_POST_TYPE)")
            let priceC = Expression<Int?>("\(Constants.KEY_PRICE)")
            let userC = Expression<String?>("\(Constants.KEY_USER)")
            let createdC = Expression<String?>("\(Constants.KEY_CREATED_DATE)")
            let modifiedC = Expression<Int?>("\(Constants.KEY_MODIFIED_DATE)")
            let statusC = Expression<String?>("\(Constants.KEY_STATUS)")
            let orderStatusC = Expression<String?>("\(Constants.KEY_ORDERSTATUS)")
            let orderIDC = Expression<String?>("\(Constants.KEY_ORDERID)")
            
            let capacity = posts.object(forKey: "capacity") != nil ? posts.object(forKey: "capacity") as! String : "0"
            let delivery = posts.object(forKey: "delivery") != nil ? posts.object(forKey: "delivery") as! Bool : false
            let gasType = posts.object(forKey: "gasType") != nil ? posts.object(forKey: "gasType") as! String : ""
            let location = posts.object(forKey: "location") != nil ? posts.object(forKey: "location") as! String : ""
            let negotiation = posts.object(forKey: "negotiation") != nil ? posts.object(forKey: "negotiation") as! Bool : false
            let postType = posts.object(forKey: "postType") != nil ? posts.object(forKey: "postType") as! String : ""
            let price = posts.object(forKey: "price") != nil ? posts.object(forKey: "price") as! String : "0"
            let user = posts.object(forKey: "user") != nil ? posts.object(forKey: "user") as! String : ""
            let created = posts.object(forKey: "createdBy") != nil ? posts.object(forKey: "createdBy") as! Int : 0
            let modified = posts.object(forKey: "modifiedBy") != nil ? posts.object(forKey: "modifiedBy") as! Int : 0
            let status = posts.object(forKey: "status") != nil ? posts.object(forKey: "status") as! String : ""
            let orderStatus = posts.object(forKey: "orderStatus") != nil ? posts.object(forKey: "orderStatus") as! String : ""
            let orderID = posts.object(forKey: "orderid") != nil ? posts.object(forKey: "orderid") as! String : ""
            
            let insert = postTable.insert(
                keyC <- postID,
                capacityC <- Int(capacity),
                deliveryC <- delivery,
                gasTypeC <- gasType,
                locationC <- location,
                negotiationC <- negotiation,
                postTypeC <- postType,
                priceC <- Int(price),
                userC <- user,
                createdC <- "\(created)",
                modifiedC <- modified,
                statusC <- status,
                orderStatusC <- orderStatus,
                orderIDC <- orderID
            )
            print(insert)
            let rowid = try db.run(insert)
            print(rowid)
        } catch {
            print("Dim background error for Subscribe Book \(error)")
        }
    }
}
