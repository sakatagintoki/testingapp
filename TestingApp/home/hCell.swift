//
//  hCell.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//  Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import UIKit

class hCell: UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblValid: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewCell.layer.shadowOpacity = 0.2
        self.viewCell.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.viewCell.layer.shadowRadius = 2
        self.viewCell.layer.shadowColor = UIColor.darkGray.cgColor
        
        self.lblCategory.layer.masksToBounds = true
        self.lblCategory.layer.cornerRadius = 28
    }
}
