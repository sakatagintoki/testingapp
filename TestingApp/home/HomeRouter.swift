//
//  HomeRouter.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - HomeRouter class
final class HomeRouter: Router {
}

// MARK: - HomeRouter API
extension HomeRouter: HomeRouterApi {
    func showPostAdd(obj: NSDictionary) {
        let module = AppModules.AddPost.build()
        module.router.show(from: _view, embedInNavController: false, setupData: obj)
    }
    
    func showPostDetail(object: NSMutableDictionary) {
        let module = AppModules.PostDetail.build()
        module.router.show(from: _view, embedInNavController: false, setupData: object)
    }
    
    func showLoginRoute() {
        let module = AppModules.login.build()
        module.router.show(from: _view)
    }
}

// MARK: - Home Viper Components
private extension HomeRouter {
    var presenter: HomePresenterApi {
        return _presenter as! HomePresenterApi
    }
}
