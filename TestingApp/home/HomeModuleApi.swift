//
//  HomeModuleApi.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Viperit

//MARK: - HomeRouter API
protocol HomeRouterApi: RouterProtocol {
    func showPostAdd(obj: NSDictionary)
    func showPostDetail(object: NSMutableDictionary)
    func showLoginRoute()
}

//MARK: - HomeView API
protocol HomeViewApi: UserInterfaceProtocol {
    func getData(_ list: NSDictionary)
}

//MARK: - HomePresenter API
protocol HomePresenterApi: PresenterProtocol {
    func postAdding(obj: NSDictionary)
    func getGasPresenter(obj: NSDictionary)
    func showPostDetails(obj: NSMutableDictionary)
    func showLoginPresenter()
}

//MARK: - HomeInteractor API
protocol HomeInteractorApi: InteractorProtocol {
    func getGasInteractor()
}
