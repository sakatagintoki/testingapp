//
//  HomePresenter.swift
//  TestingApp
//
//  Created by YGN User on 20/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - HomePresenter Class
final class HomePresenter: Presenter {
    override func viewHasLoaded() {
        super.viewHasLoaded()
        interactor.getGasInteractor()
    }
}

// MARK: - HomePresenter API
extension HomePresenter: HomePresenterApi {
    func getGasPresenter(obj: NSDictionary) {
        view.getData(obj)
    }
    
    func postAdding(obj: NSDictionary) {
        router.showPostAdd(obj: obj)
    }
    
    func showPostDetails(obj: NSMutableDictionary) {
        router.showPostDetail(object: obj)
    }
    
    func showLoginPresenter() {
        router.showLoginRoute()
    }
}

// MARK: - Home Viper Components
private extension HomePresenter {
    var view: HomeViewApi {
        return _view as! HomeViewApi
    }
    var interactor: HomeInteractorApi {
        return _interactor as! HomeInteractorApi
    }
    var router: HomeRouterApi {
        return _router as! HomeRouterApi
    }
}
