//
//  AddPostDisplayData.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit
import Firebase
import FirebaseDatabase

// MARK: - AddPostDisplayData class
final class AddPostDisplayData: DisplayData {
    
}

class AddPostDataManager: NSObject {
    var ref: DatabaseReference!
    func getGasList(_ complete: @escaping (NSDictionary) -> Void) {
        ref = Database.database().reference()
        let recentPostsQuery1 = (ref?.child("gasType"))
        
        recentPostsQuery1?.observe(DataEventType.value, with: { (snapshot) in
            let gasArray = snapshot.value as! NSDictionary
            complete(gasArray)
        })
    }
}
