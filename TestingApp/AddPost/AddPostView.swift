//
//  AddPostView.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import UIKit
import Viperit
import Firebase
import FirebaseDatabase
import DropDown
import DLRadioButton

//MARK: AddPostView Class
final class AddPostView: UserInterface {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblGasType: UILabel!
    @IBOutlet weak var buttonGasType: UIButton!
    @IBOutlet weak var lblCapacity: UILabel!
    @IBOutlet weak var txtCapacity: UITextField!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet var buttonDelivery: UIButton!
    @IBOutlet var btnWTS: DLRadioButton!
    @IBOutlet var btnWTB: DLRadioButton!
    @IBOutlet var buttonAddPost: UIButton!
    
    var gasArray = NSDictionary()
    var gasAdded: [String] = []
    var gasArrayList: NSArray = []
    var getObj = NSMutableDictionary()
    var gasType: String = ""
    var gasValue: String = ""
    var postID: String = ""
    var postType: String = "1"
    var delivery: Bool = false
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.localization()
        
        self.ref = Database.database().reference()
        
        if self.getObj.count > 0 {
            let key = self.getObj.allKeys[0] as! String
            let object = self.getObj.object(forKey: key) as! NSDictionary
            let gasType = object.object(forKey: "gasType") as! String
            let pType = object.object(forKey: "postType") as! String
            self.buttonGasType.setTitle(gasType, for: .normal)
            self.txtCapacity.text = object.object(forKey: "capacity") as? String
            self.txtPrice.text = object.object(forKey: "price") as? String
            self.txtAddress.text = object.object(forKey: "location") as? String
            self.delivery = object.object(forKey: "delivery") as! Bool
            if self.delivery == true {
                let image = UIImage.init(named: "selected.png")
                self.buttonDelivery.setBackgroundImage(image, for: .normal)
            } else {
                let image = UIImage.init(named: "selected_not.png")
                self.buttonDelivery.setBackgroundImage(image, for: .normal)
            }
            if pType == "1" {
                self.btnWTS.isSelected = true
            } else {
                self.btnWTB.isSelected = true
            }
            self.buttonAddPost.setTitle("Edit Post", for: .normal)
        } else {
            self.postType = ""
            self.txtCapacity.text = ""
            self.txtCapacity.text = ""
            self.txtPrice.text = ""
            self.txtAddress.text = ""
            self.buttonAddPost.setTitle("Add Post", for: .normal)
        }
        
        let pwdBorder = CALayer()
        pwdBorder.borderColor = UIColor.gray.cgColor
        pwdBorder.frame = CGRect(x: 0, y: self.viewHeader.frame.size.height - 1, width: self.viewHeader.frame.size.width, height: 0.5)
        pwdBorder.borderWidth = 1
        pwdBorder.opacity = 0.4
        self.viewHeader.layer.addSublayer(pwdBorder)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func clickAddPost(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        let user = firebaseAuth.currentUser
        let capacity = self.txtCapacity.text! as String
        let price = self.txtPrice.text! as String
        let address = self.txtAddress.text! as String
        let uuid = user?.uid
        let timestamp = ServerValue.timestamp()
        if !(self.txtCapacity.text?.isEmpty)! && !(self.txtPrice.text?.isEmpty)! && !(self.txtAddress.text?.isEmpty)! {
            let posts = ["gasType": self.gasValue, "capacity": capacity, "price": price, "location": address, "postType" : "\(self.postType)", "delivery" : self.delivery, "createdBy": timestamp, "user": uuid!, "modifiedBy": timestamp, "status": "Active"] as [String : Any]
            if (self.getObj as AnyObject).count > 0 {
                self.ref.child("posts").child("\(self.postID)").setValue(posts){ (error, ref) -> Void in
                    print("finished updating")
                    if let error = error {
                        print(error.localizedDescription)
                        let alert = Utility().showAlert("Error", message: "Post Updating Problem", btnText: "OK")
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alertController = UIAlertController(title: "Post Updating", message: "Successfully Post Adding", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
                            (result : UIAlertAction) -> Void in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            } else {
                self.ref.child("posts").childByAutoId().setValue(posts) { (error, ref) -> Void in
                    if let error = error {
                        print(error.localizedDescription)
                        let alert = Utility().showAlert("Error", message: "Post Inserting Problem", btnText: "OK")
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alertController = UIAlertController(title: "Post Adding", message: "Successfully Post Adding", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                            (result : UIAlertAction) -> Void in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        } else {
            let alert = Utility().showAlert("", message: "Please Fill Every TextField.", btnText: "OK")
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func selectedButton(_ sender: DLRadioButton) {
        if sender.selected()!.titleLabel!.text == "Want to Sell" {
            self.postType = "1"
        } else {
            self.postType = "2"
        }
    }
    
    @IBAction func clickGasType(_ sender: Any) {
        let dropDown = DropDown()
        // The view to which the drop down will appear on
        dropDown.anchorView = self.buttonGasType // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = self.gasAdded
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.buttonGasType.setTitle(item, for: .normal)
            self.gasType = item
            let gasV = self.gasArrayList[index] as! NSDictionary
            self.gasValue = gasV.object(forKey: "keyword") as! String
            dropDown.hide()
        }
        dropDown.show()
    }
    
    @IBAction func clickClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickDelivery(_ sender: Any) {
        if self.delivery == true {
            let image = UIImage.init(named: "selected_not.png")
            self.buttonDelivery.setBackgroundImage(image, for: .normal)
            self.delivery = false
        } else {
            let image = UIImage.init(named: "selected.png")
            self.buttonDelivery.setBackgroundImage(image, for: .normal)
            self.delivery = true
        }
    }
    
    func localization(){
        self.lblGasType.text = NSLocalizedString("STR_GASTYPE",comment:"")
        self.lblPrice.text = NSLocalizedString("STR_PRICE",comment:"")
        self.lblAddress.text = NSLocalizedString("STR_ADDRESS",comment:"")
        self.btnWTS.setTitle(NSLocalizedString("STR_WANTTOSELL",comment:""), for: .normal)
        self.btnWTB.setTitle(NSLocalizedString("STR_WANTTOBUY",comment:""), for: .normal)
    }
}

//MARK: - AddPostView API
extension AddPostView: AddPostViewApi {
    func showPost(_ gasObj: NSDictionary) {
        self.gasArray = gasObj
        self.gasArrayList = self.gasArray.allValues as NSArray
        let gasObj = self.gasArray.allValues[0] as! NSDictionary
        let gasTitle = gasObj.object(forKey: "name") != nil ? gasObj.object(forKey: "name") as! String : ""
        let gasKeyword = gasObj.object(forKey: "keyword") != nil ? gasObj.object(forKey: "keyword") as! String : ""
        self.buttonGasType.setTitle(gasTitle, for: .normal)
        self.gasType = "\(gasKeyword)"
        for i in 0..<gasArrayList.count {
            let gObj = gasArrayList[i] as! NSDictionary
            let gTitle = gObj.object(forKey: "name") as! String
            self.gasAdded.append(gTitle)
        }
    }
    
    func getData(_ data: Any){
        if (data as AnyObject).count > 0 {
            self.getObj = data as! NSMutableDictionary
            self.postID = self.getObj.allKeys[0] as! String
        }
    }
}

// MARK: - AddPostView Viper Components API
private extension AddPostView {
    var presenter: AddPostPresenterApi {
        return _presenter as! AddPostPresenterApi
    }
    var displayData: AddPostDisplayData {
        return _displayData as! AddPostDisplayData
    }
}
