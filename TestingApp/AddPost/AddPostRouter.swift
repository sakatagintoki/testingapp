//
//  AddPostRouter.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - AddPostRouter class
final class AddPostRouter: Router {
}

// MARK: - AddPostRouter API
extension AddPostRouter: AddPostRouterApi {
}

// MARK: - AddPost Viper Components
private extension AddPostRouter {
    var presenter: AddPostPresenterApi {
        return _presenter as! AddPostPresenterApi
    }
}
