//
//  AddPostPresenter.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - AddPostPresenter Class
final class AddPostPresenter: Presenter {
    
    override func setupView(data: Any) {
        view.getData(data)
    }
    
    override func viewHasLoaded() {
        super.viewHasLoaded()
        interactor.getGasInteractor()
    }
}

// MARK: - AddPostPresenter API
extension AddPostPresenter: AddPostPresenterApi {
    func getGasPresenter(_ gasObj: NSDictionary){
        view.showPost(gasObj)
    }
}

// MARK: - AddPost Viper Components
private extension AddPostPresenter {
    var view: AddPostViewApi {
        return _view as! AddPostViewApi
    }
    var interactor: AddPostInteractorApi {
        return _interactor as! AddPostInteractorApi
    }
    var router: AddPostRouterApi {
        return _router as! AddPostRouterApi
    }
}
