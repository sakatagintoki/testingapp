//
//  AddPostInteractor.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Foundation
import Viperit

// MARK: - AddPostInteractor Class
final class AddPostInteractor: Interactor {
    // Business Layer
    func getGasInteractor() {
        AddPostDataManager().getGasList(){
            response in
            self.presenter.getGasPresenter(response)
        }
    }
}

// MARK: - AddPostInteractor API
extension AddPostInteractor: AddPostInteractorApi {
}

// MARK: - Interactor Viper Components Api
private extension AddPostInteractor {
    var presenter: AddPostPresenterApi {
        return _presenter as! AddPostPresenterApi
    }
}
