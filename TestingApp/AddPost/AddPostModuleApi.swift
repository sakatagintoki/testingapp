//
//  AddPostModuleApi.swift
//  TestingApp
//
//  Created by YGN User on 21/1/18.
//Copyright © 2018 TharYar. All rights reserved.
//

import Viperit

//MARK: - AddPostRouter API
protocol AddPostRouterApi: RouterProtocol {
}

//MARK: - AddPostView API
protocol AddPostViewApi: UserInterfaceProtocol {
    func showPost(_ gasObj: NSDictionary)
    func getData(_ data: Any)
}

//MARK: - AddPostPresenter API
protocol AddPostPresenterApi: PresenterProtocol {
    func getGasPresenter(_ gasObj: NSDictionary)
}

//MARK: - AddPostInteractor API
protocol AddPostInteractorApi: InteractorProtocol {
    func getGasInteractor()
}
